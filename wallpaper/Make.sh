#! /bin/bash

# WALLPAPER FULL HD = "1920x1080"
inkscape -z -i fullhd -w 1920 -h 1080 -e wallpaper_1920x1080.png wallpaper.svg

# WALLPAPER WIDE = "1920x1200 2560x1600"
inkscape -z -i wide -w 1920 -h 1200 -e wallpaper_1920x1200.png wallpaper.svg
inkscape -z -i wide -w 2560 -h 1600 -e wallpaper_2560x1600.png wallpaper.svg

# WALLPAPER NORMAL = "1024x768 1600x1200 2400x1800"
inkscape -z -i normal -w 1024 -h 768 -e wallpaper_1024x768.png wallpaper.svg
inkscape -z -i normal -w 1600 -h 1200 -e wallpaper_1600x1200.png wallpaper.svg
inkscape -z -i normal -w 2400 -h 1800 -e wallpaper_2400x1800.png wallpaper.svg

# GENERATOR KSPLASH STRUCTURE
mkdir -p ksplash
cp ksplash.rc ksplash/Theme.rc
convert -resize 400x300 wallpaper_1024x768.png ksplash/Preview.png
mkdir -p ksplash/1920x1080
cp -a wallpaper_1920x1080.png ksplash/1920x1080/background.png
mkdir -p ksplash/2400x1800
cp -a wallpaper_2400x1800.png ksplash/2400x1800/background.png
cp -a ksplash-animation/* ksplash/2400x1800/
mkdir -p ksplash/1600x1200
cp -a wallpaper_1600x1200.png ksplash/1600x1200/background.png
cp -a ksplash-animation/* ksplash/1600x1200/
mkdir -p ksplash/1024x768
cp -a wallpaper_1024x768.png ksplash/1024x768/background.png
mkdir -p ksplash/2560x1600
cp -a wallpaper_2560x1600.png ksplash/2560x1600/background.png
mkdir -p ksplash/1920x1200
cp -a wallpaper_1920x1200.png ksplash/1920x1200/background.png

